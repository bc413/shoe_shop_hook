import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./Page/Home/Home";
import Ex_Shoe_Shop_Hook from "./Page/Ex_Shoe_Shop_Hook/Ex_Shoe_Shop_Hook";
import Header from "./Components/Header";

function App() {
  return (
    <div className="text-center container">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/Ex_Shoe_Shop_Hook" element={<Ex_Shoe_Shop_Hook />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
