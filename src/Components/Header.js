import React from "react";
import { NavLink } from "react-router-dom";

export default function Header() {
  return (
    <div className="text-center">
      <button className="btn btn-primary mx-5">
        <NavLink className={"text-white"} to="/">
          Home
        </NavLink>
      </button>
      <button className="btn btn-primary mx-5">
        <NavLink className={"text-white"} to="/Ex_Shoe_Shop_Hook">
          Ex Shoe Shop
        </NavLink>
      </button>
    </div>
  );
}
