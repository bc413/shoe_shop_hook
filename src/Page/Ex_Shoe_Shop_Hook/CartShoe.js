import React from "react";
import { connect } from "react-redux";

function CartShoe(props) {
  let renderCart = () => {
    return props.data.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img style={{ width: "100px" }} src={item.image} alt="" />
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => {
                props.handleUpdateSL(item.id, 1);
              }}
              className="btn btn-primary"
            >
              +
            </button>
            <strong className="mx-2">{item.soLuong}</strong>
            <button
              onClick={() => {
                props.handleUpdateSL(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                props.handleDelete(item.id);
              }}
              className="btn btn-danger"
            >
              DELETE
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <h5>CartShoe</h5>
      <table className="w-100">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Img</th>
            <th>Price</th>
            <th>SL</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{renderCart()}</tbody>
      </table>
    </div>
  );
}

let mapStateToProps = (sate) => {
  return {
    data: sate.shoeReducer.cart,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleUpdateSL: (idShoe, number) => {
      let action = {
        type: "CAP_NHAT_SO_LUONG",
        payload: { idShoe, number },
      };
      dispatch(action);
    },
    handleDelete: (idShoe) => {
      let action = {
        type: "DELETE",
        payload: idShoe,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
