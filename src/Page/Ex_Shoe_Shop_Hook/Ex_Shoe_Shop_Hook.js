import React from "react";
import CartShoe from "./CartShoe";
import ListShoe from "./ListShoe";

export default function Ex_Shoe_Shop_Hook() {
  return (
    <div className="row">
      <div className="col-7">
        <CartShoe />
      </div>
      <div className="col-5">
        <ListShoe />
      </div>
    </div>
  );
}
