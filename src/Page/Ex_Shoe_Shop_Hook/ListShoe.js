import React from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

function ListShoe(props) {
  let renderShoeShop = () => {
    return props.data.map((item, index) => {
      return <ItemShoe item={item} key={index} />;
    });
  };
  return (
    <div>
      <h5>ListShoe</h5>
      <div className="row">{renderShoeShop()}</div>
    </div>
  );
}

let mapStateToProps = (state) => {
  return {
    data: state.shoeReducer.dataShoe,
  };
};

export default connect(mapStateToProps)(ListShoe);
