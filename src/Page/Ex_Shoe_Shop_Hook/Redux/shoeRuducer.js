import { data_shoe } from "../DataShoe";

let initiaValu = {
  dataShoe: data_shoe,
  cart: [],
};

export let Shoe_Shop_Reducer = (state = initiaValu, action) => {
  switch (action.type) {
    case "THEM SAN PHAM": {
      let newCart = [...state.cart];
      let index = newCart.findIndex((item) => {
        return item.id === action.payLoad.id;
      });
      let newObjec = { ...action.payLoad, soLuong: 1 };
      if (index == -1) {
        newCart.push(newObjec);
      } else {
        newCart[index].soLuong++;
      }
      return { ...state, cart: newCart };
    }
    case "CAP_NHAT_SO_LUONG": {
      let newCart = [...state.cart];
      let index = newCart.findIndex((item) => {
        return item.id === action.payload.idShoe;
      });
      if (index != -1) {
        newCart[index].soLuong += action.payload.number;
      }
      if (newCart[index].soLuong === 0) {
        newCart.splice(index, 1);
      }
      return { ...state, cart: newCart };
    }
    case "DELETE": {
      let newCart = [...state.cart];
      let arrNew = newCart.filter((item) => {
        return item.id != action.payload;
      });
      console.log(arrNew);
      return { ...state, cart: arrNew };
    }
    default:
      return state;
  }
};
