import React from "react";
import { Card } from "antd";
import { connect } from "react-redux";
const { Meta } = Card;

function ItemShoe(props) {
  return (
    <Card
      className="col-4"
      hoverable
      style={{
        width: 240,
      }}
      cover={<img alt="example" src={props.item.image} />}
    >
      <Meta title={props.item.name} description="www.instagram.com" />
      <p>{props.item.price}$</p>
      <button
        onClick={() => {
          props.handeleAdd(props.item);
        }}
        className="btn btn-primary"
      >
        Add
      </button>
    </Card>
  );
}

let mapDispatchToProps = (dispath) => {
  return {
    handeleAdd: (shoe) => {
      let action = {
        type: "THEM SAN PHAM",
        payLoad: shoe,
      };
      dispath(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemShoe);
